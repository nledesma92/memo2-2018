class Isbn10

  def is_valid(input)
    input = clean_input(input)

    if (!check_valid_characters(input))
      return false
    elsif (!check_length(input))
      return false
    else 
      return check_sum(input)
    end
  end

  def clean_input(input)
    input_no_spaces = input.gsub(/\s+/, '')
    input_no_hyphens = input_no_spaces.gsub(/-+/, '')
    return input_no_hyphens
  end

  def check_valid_characters(clean_input)
    first_9_char_ok = ((clean_input[0..8] =~ /^\d{9,9}$/)  == 0)
    last_char_ok = ((clean_input[9] =~ /^[0-9X]$/) == 0 )
    return first_9_char_ok && last_char_ok
  end

  def check_length(clean_input)
    return clean_input.length == 10
  end

  def check_sum(clean_input)
    sum = 0
    aux_array = clean_input.split('')
    
    for i in 0..aux_array.count - 2
      sum += (i+1) * aux_array[i].to_i
    end
    mod_11 = sum % 11

    if (mod_11 == 10)
      mod_11 = 'X'
    else 
      mod_11 = mod_11.to_s
    end
    return clean_input[9] == mod_11
  end

end