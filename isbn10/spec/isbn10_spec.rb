# Consigna: 
# ISBN-10 is made up of 9 digits plus a check digit (which may be 'X'). Spaces and hyphens 
# may be included in a code, but are not significant. This means that 9780471486480 is 
# equivalent to 978-0-471-48648-0 and 978 0 471 48648 0.
#
# The check digit for ISBN-10 is calculated by multiplying each digit by its position 
# (i.e., 1 x 1st digit, 2 x 2nd digit, etc.), summing these products together and taking 
# modulo 11 of the result (with 'X' being used if the result is 10).
# 
# Valid examples: 0471958697, 0 471 95869 7, 0-471-95869-7, 155404295X
# Invalid examples: 
# * 0471958698 (wrong verifier)
# * 0471958 (wrong length)
# * a471958697 (wrong digit)

# Write logic to validate if a given string is a valid ISBN-10
 
require 'rspec' 
require_relative '../model/isbn10'

describe 'Isbn10' do

  let(:isbn10) { Isbn10.new }  

  it 'is_valid verifica que 0471958 es un string isbn10 incorrecto, porque el largo es invalido' do
    expect(isbn10.is_valid('0471958')).to eq false
  end

  it 'is_valid verifica que 04 719 5 8 es un string isbn10 incorrecto, porque el largo (sin espacios) es invalido' do
    expect(isbn10.is_valid('04 719 5 8')).to eq false
  end
  
  it 'is_valid verifica que 04-719-5-8 es un string isbn10 incorrecto, porque el largo (sin espacios) es invalido' do
    expect(isbn10.is_valid('04-719-5-8')).to eq false
  end

  it 'is_valid verifica que 0471958697 es un string isbn10 correcto' do
    expect(isbn10.is_valid('0471958697')).to eq true
  end

  it 'is_valid verifica que 047a958697 es un string isbn10 incorrecto (tiene caracteres invalidos)' do
    expect(isbn10.is_valid('047a958697')).to eq false
  end

  it 'is_valid verifica que 155404295X es un string isbn10 correcto' do
    expect(isbn10.is_valid('155404295X')).to eq true
  end

  it 'is_valid verifica que 155404295X es un string isbn10 incorrecto (tiene el ultimo caracter invalido)' do
    expect(isbn10.is_valid('155404295E')).to eq false
  end

  it 'is_valid verifica que "0471958698" es un string isbn10 incorrecto' do
    expect(isbn10.is_valid('0471958698')).to eq false
  end
  
  it 'is_valid verifica que 0-471-95869-7 es un string isbn10 correcto' do
    expect(isbn10.is_valid('0-471-95869-7')).to eq true
  end

  it 'is_valid verifica que 0-943396-04-2 es un string isbn10 correcto' do
    expect(isbn10.is_valid('0-943396-04-2')).to eq true
  end

end
