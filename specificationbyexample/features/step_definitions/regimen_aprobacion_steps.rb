Given(/^el alumno "([^"]*)"$/) do |arg1|
  @alumno = Alumno.new(arg1)
  @materia = Materia.new()
end

Given(/^la tareas individual "([^"]*)"$/) do |arg1|
  @materia.add_tarea(arg1)
end

Given(/^el proyecto grupal "([^"]*)"$/) do |arg1|
  @materia.add_proyecto_grupal(arg1)
end

Given(/^el alumno regular "([^"]*)"$/) do |arg1|
  # Step completado previamente en el background
end

When(/^aprobo todas las tareas semanales$/) do
  @materia.tareas.each do |tarea|
    @materia.aprobar_tarea_a_alumno(@alumno, tarea.nombre)
  end
end

When(/^asistio al (\d+) % de las clases$/) do |arg1|
  @materia.set_asistencia_alumno(@alumno, arg1)
end

When(/^aprobo al menos (\d+) iteraciones del proyecto "([^"]*)"$/) do |arg1, arg2|
  i = 0
  while i < arg1.to_i  do
    @materia.aprobar_iteracion_proyecto_gruapl_alumno(@alumno, arg2)
    i += 1
  end
end

Then(/^aprobo la materia$/) do
  expect(@materia.get_aprobo_alumno(@alumno)).to eq true
end

Then(/^No aprobo la materia$/) do
  expect(@materia.get_aprobo_alumno(@alumno)).to eq false
end

When(/^no aprobo la tarea individual "([^"]*)"$/) do | arg1 |
  @materia.tareas.each do |tarea|
    if tarea.nombre != arg1
      @materia.aprobar_tarea_a_alumno(@alumno, tarea.nombre)
    end
  end
end

When(/^aprobo (\d+) iteraciones del proyecto "([^"]*)"$/) do |arg1, arg2|
  i = 0
  while i < arg1.to_i  do
    @materia.aprobar_iteracion_proyecto_gruapl_alumno(@alumno, arg2)
    i += 1
  end
end

