class Tarea
  attr_reader :nombre
  attr_reader :alumnos_aprobados

  def initialize(nombre)
    @nombre = nombre
    @alumnos_aprobados = Array.new
  end
  
  def aprobar_alumno(alumno)
    @alumnos_aprobados.push(alumno)
  end

  def get_aprobo_alumno(alumno)
    return @alumnos_aprobados.include?(alumno)
  end
end