
class Materia
  LIMITE_ASISTENCIA_APROBACION = 75
  
  attr_reader :tareas
  attr_reader :proyectos_grupales
  attr_reader :planilla_asistencia
  
  def initialize()
    @tareas = Array.new
    @proyectos_grupales = Array.new
    @planilla_asistencia = Hash.new
  end

  def add_tarea(nombre_tarea)
    tarea = Tarea.new(nombre_tarea)
    @tareas.push(tarea)
  end

  def add_proyecto_grupal(nombre_proyecto)
    proyecto_grupal = ProyectoGrupal.new(nombre_proyecto)
    @proyectos_grupales.push(proyecto_grupal)
  end

  def aprobar_tarea_a_alumno(alumno, nombre_tarea)
    @tareas.each do |tarea|
      if (tarea.nombre == nombre_tarea)
        tarea.aprobar_alumno(alumno)
        return tarea
      end
    end
  end

  def set_asistencia_alumno(alumno, asistencia)
    @planilla_asistencia[alumno] = asistencia.to_i
  end

  def aprobar_iteracion_proyecto_gruapl_alumno(alumno, nombre_proyecto)
    @proyectos_grupales.each do |proyecto|
      if (proyecto.nombre == nombre_proyecto)
        proyecto.aprobar_iteracion_alumno(alumno)
        return proyecto
      end
    end
  end

  def get_asistencia_alumno(alumno)
    return @planilla_asistencia.member?(alumno) ? @planilla_asistencia[alumno] : 0    
  end

  def get_aprobo_alumno(alumno)
    return check_asistencia_ok(alumno) && check_tareas_ok(alumno) && check_proyectos_ok(alumno)
  end

  private

  def check_asistencia_ok(alumno)
    return get_asistencia_alumno(alumno) >= LIMITE_ASISTENCIA_APROBACION
  end

  def check_tareas_ok(alumno)
    @tareas.each do |tarea|
      if (!tarea.get_aprobo_alumno(alumno))
        return false
      end
    end
    return true
  end

  def check_proyectos_ok(alumno)
    @proyectos_grupales.each do |proyecto|
      if (!proyecto.get_aprobo_alumno(alumno))
        return false
      end
    end
    return true
  end
end