class ProyectoGrupal
  LIMITE_ITERACIONES_APROBACION = 3

  attr_reader :nombre
  attr_reader :iteraciones_aprobadas_por_alumno

  def initialize(nombre)
    @nombre = nombre
    @iteraciones_aprobadas_por_alumno = Hash.new
  end

  def aprobar_iteracion_alumno(alumno)
    if @iteraciones_aprobadas_por_alumno.member?(alumno)
      @iteraciones_aprobadas_por_alumno[alumno] = @iteraciones_aprobadas_por_alumno[alumno] + 1
    else
      @iteraciones_aprobadas_por_alumno[alumno] = 1      
    end
  end

  def get_iteraciones_aprobadas_alumno(alumno)
    return @iteraciones_aprobadas_por_alumno.member?(alumno) ? @iteraciones_aprobadas_por_alumno[alumno] : 0
  end

  def get_aprobo_alumno(alumno)
    return get_iteraciones_aprobadas_alumno(alumno) >= LIMITE_ITERACIONES_APROBACION
  end

end
