require 'rspec' 
require_relative '../model/proyecto_grupal'
require_relative '../model/alumno'

describe 'ProyectoGrupal' do

  let(:proyecto_grupal) { ProyectoGrupal.new('ph') }  
   
  it 'cuando apruebo 2 iteraciones a un alumno, cuando consulto sus iteraciones aprobadas debe retornar 2' do
    alumno = Alumno.new('Juan')
    proyecto_grupal.aprobar_iteracion_alumno(alumno)
    proyecto_grupal.aprobar_iteracion_alumno(alumno)
    expect(proyecto_grupal.get_iteraciones_aprobadas_alumno(alumno)).to eq 2
  end

  it 'cuando aun no apruebo ninguna iteracion al alumno, cuando consulto sus iteraciones aprobadas debe retornar 0' do
    alumno = Alumno.new('Juan')
    expect(proyecto_grupal.get_iteraciones_aprobadas_alumno(alumno)).to eq 0
  end

  it 'cuando apruebo 3 iteraciones a un alumno, el proyecto debe figurar como aprobado' do
    alumno = Alumno.new('Juan')
    proyecto_grupal.aprobar_iteracion_alumno(alumno)
    proyecto_grupal.aprobar_iteracion_alumno(alumno)
    proyecto_grupal.aprobar_iteracion_alumno(alumno)
    expect(proyecto_grupal.get_aprobo_alumno(alumno)).to eq true
  end

  it 'cuando apruebo 2 iteraciones a un alumno, el proyecto debe figurar como no aprobado' do
    alumno = Alumno.new('Juan')
    proyecto_grupal.aprobar_iteracion_alumno(alumno)
    proyecto_grupal.aprobar_iteracion_alumno(alumno)
    expect(proyecto_grupal.get_aprobo_alumno(alumno)).to eq false
  end
end