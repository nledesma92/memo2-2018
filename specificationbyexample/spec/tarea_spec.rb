require 'rspec' 
require_relative '../model/tarea'
require_relative '../model/alumno'

describe 'Tarea' do

  let(:tarea) { Tarea.new('t') }  
   
  it 'cuando apruebo a un estudiante, si consulto su aprobacion debe figurar como aprobado' do
    alumno = Alumno.new('Juan')
    tarea.aprobar_alumno(alumno)
    expect(tarea.get_aprobo_alumno(alumno)).to eq true
  end

  it 'cuando aun no apruebo a un estudiante, si consulto su aprobacion debe no figurar como aprobado' do
    alumno = Alumno.new('Juan')
    expect(tarea.get_aprobo_alumno(alumno)).to eq false
  end
end