require 'rspec' 
require_relative '../model/materia'
require_relative '../model/proyecto_grupal'
require_relative '../model/tarea'
require_relative '../model/alumno'

describe 'Materia' do

  let(:materia) { Materia.new }  
   
  it 'cuando le asigno 75 % de asistencia a un alumno, luego debe devolverme la correcta (75)' do
    alumno = Alumno.new('Juan')
    materia.set_asistencia_alumno(alumno, 75)
    expect(materia.get_asistencia_alumno(alumno)).to eq 75
  end

  it 'cuando no asigno asistencia a un alumno debe retornar 0' do
    alumno = Alumno.new('Juan')
    expect(materia.get_asistencia_alumno(alumno)).to eq 0
  end

  it 'cuando un alumno tiene menos de 75 % de asistencia no debe figurar como aprobado' do
    alumno = Alumno.new('Juan')
    materia.set_asistencia_alumno(alumno, 60)
    expect(materia.get_aprobo_alumno(alumno)).to eq false
  end

  it 'cuando un alumno aprueba todas las tareas, y cumple el 75 % de la asistencia, debe figurar como aprobado' do
    alumno = Alumno.new('Juan')
    materia.add_tarea('t1')
    materia.add_tarea('t2')
    materia.set_asistencia_alumno(alumno, 75)
    materia.aprobar_tarea_a_alumno(alumno, 't1')
    materia.aprobar_tarea_a_alumno(alumno, 't2')
    expect(materia.get_aprobo_alumno(alumno)).to eq true
  end

  it 'cuando un alumno no aprueba todas las tareas, y cumple el 75 % de la asistencia, debe figurar como no aprobado' do
    alumno = Alumno.new('Juan')
    materia.add_tarea('t1')
    materia.add_tarea('t2')
    materia.set_asistencia_alumno(alumno, 75)
    materia.aprobar_tarea_a_alumno(alumno, 't1')
    expect(materia.get_aprobo_alumno(alumno)).to eq false
  end

  it 'cuando un alumno aprueba todas las tareas, cumple con el 75 % de la asistencia, y aprueba 3 iteraciones del proyecto grupal, 
    debe figurar como aprobado' do
    alumno = Alumno.new('Juan')
    materia.add_tarea('t1')
    materia.add_tarea('t2')
    materia.add_proyecto_grupal('pg')
    materia.set_asistencia_alumno(alumno, 75)
    materia.aprobar_tarea_a_alumno(alumno, 't1')
    materia.aprobar_tarea_a_alumno(alumno, 't2')
    materia.aprobar_iteracion_proyecto_gruapl_alumno(alumno, 'pg')
    materia.aprobar_iteracion_proyecto_gruapl_alumno(alumno, 'pg')
    materia.aprobar_iteracion_proyecto_gruapl_alumno(alumno, 'pg')
    expect(materia.get_aprobo_alumno(alumno)).to eq true
  end

  it 'cuando un alumno aprueba todas las tareas, cumple con el 75 % de la asistencia, y aprueba solo 2 iteraciones del proyecto grupal, 
  debe figurar como no aprobado' do
  alumno = Alumno.new('Juan')
  materia.add_tarea('t1')
  materia.add_tarea('t2')
  materia.add_proyecto_grupal('pg')
  materia.set_asistencia_alumno(alumno, 75)
  materia.aprobar_tarea_a_alumno(alumno, 't1')
  materia.aprobar_tarea_a_alumno(alumno, 't2')
  materia.aprobar_iteracion_proyecto_gruapl_alumno(alumno, 'pg')
  materia.aprobar_iteracion_proyecto_gruapl_alumno(alumno, 'pg')
  expect(materia.get_aprobo_alumno(alumno)).to eq false
end
end