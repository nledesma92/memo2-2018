# spec/app_spec.rb

require File.expand_path '../spec_helper.rb', __FILE__

NOT_FOUND = 404
SUCCESS = 200

describe "Calculadora de descuentos" do

  it "get /total_neto?precio=500&cantidad=3&estado=UT retorna un total de 1554.6675" do
    precio = 500
    cantidad = 3
    estado = 'UT'
    expected_total = 1554.6675
    get "/total_neto?precio=#{precio}&cantidad=#{cantidad}&estado=#{estado}"
    expect(last_response).to be_ok
    expect(last_response.status).to eq SUCCESS
    expect(JSON.parse(last_response.body)["detalle"]["precio"]).to eq precio
    expect(JSON.parse(last_response.body)["detalle"]["cantidad"]).to eq cantidad
    expect(JSON.parse(last_response.body)["detalle"]["estado"]).to eq estado
    expect(JSON.parse(last_response.body)["total"]).to be_within(0.001).of(expected_total)
  end

  it "get /total_neto?precio=1000&cantidad=2&estado=NV retorna un total de 2095.2" do
    precio = 1000
    cantidad = 2
    estado = 'NV'
    expected_total = 2095.2
    get "/total_neto?precio=#{precio}&cantidad=#{cantidad}&estado=#{estado}"
    expect(last_response).to be_ok
    expect(last_response.status).to eq SUCCESS
    expect(JSON.parse(last_response.body)["detalle"]["precio"]).to eq precio
    expect(JSON.parse(last_response.body)["detalle"]["cantidad"]).to eq cantidad
    expect(JSON.parse(last_response.body)["detalle"]["estado"]).to eq estado
    expect(JSON.parse(last_response.body)["total"]).to be_within(0.001).of(expected_total)
  end

  it "get /total_neto?precio=1000&cantidad=3&estado=AL retorna un total de 3026.4" do
    precio = 1000
    cantidad = 3
    estado = 'AL'
    expected_total = 3026.4
    get "/total_neto?precio=#{precio}&cantidad=#{cantidad}&estado=#{estado}"
    expect(last_response).to be_ok
    expect(last_response.status).to eq SUCCESS
    expect(JSON.parse(last_response.body)["detalle"]["precio"]).to eq precio
    expect(JSON.parse(last_response.body)["detalle"]["cantidad"]).to eq cantidad
    expect(JSON.parse(last_response.body)["detalle"]["estado"]).to eq estado
    expect(JSON.parse(last_response.body)["total"]).to be_within(0.001).of(expected_total)
  end

  it "get /total_neto?precio=2000&cantidad=3&estado=CA retorna un total de 6170.25" do
    precio = 2000
    cantidad = 3
    estado = 'CA'
    expected_total = 6170.25
    get "/total_neto?precio=#{precio}&cantidad=#{cantidad}&estado=#{estado}"
    expect(last_response).to be_ok
    expect(last_response.status).to eq SUCCESS
    expect(JSON.parse(last_response.body)["detalle"]["precio"]).to eq precio
    expect(JSON.parse(last_response.body)["detalle"]["cantidad"]).to eq cantidad
    expect(JSON.parse(last_response.body)["detalle"]["estado"]).to eq estado
    expect(JSON.parse(last_response.body)["total"]).to be_within(0.001).of(expected_total)
  end

  it "get /total_neto?precio=5000&cantidad=2&estado=TX retorna un total de 9562.5" do
    precio = 5000
    cantidad = 2
    estado = 'TX'
    expected_total = 9562.5
    get "/total_neto?precio=#{precio}&cantidad=#{cantidad}&estado=#{estado}"
    expect(last_response).to be_ok
    expect(last_response.status).to eq SUCCESS
    expect(JSON.parse(last_response.body)["detalle"]["precio"]).to eq precio
    expect(JSON.parse(last_response.body)["detalle"]["cantidad"]).to eq cantidad
    expect(JSON.parse(last_response.body)["detalle"]["estado"]).to eq estado
    expect(JSON.parse(last_response.body)["total"]).to be_within(0.001).of(expected_total)
  end

  it "get /total_neto?precio=20000&cantidad=3&estado=CA retorna un total de 55207.5" do
    precio = 20000
    cantidad = 3
    estado = 'CA'
    expected_total = 55207.5
    get "/total_neto?precio=#{precio}&cantidad=#{cantidad}&estado=#{estado}"
    expect(last_response).to be_ok
    expect(last_response.status).to eq SUCCESS
    expect(JSON.parse(last_response.body)["detalle"]["precio"]).to eq precio
    expect(JSON.parse(last_response.body)["detalle"]["cantidad"]).to eq cantidad
    expect(JSON.parse(last_response.body)["detalle"]["estado"]).to eq estado
    expect(JSON.parse(last_response.body)["total"]).to be_within(0.001).of(expected_total)
  end

end


