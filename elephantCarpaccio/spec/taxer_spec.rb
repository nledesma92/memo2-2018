require 'rspec' 
require_relative '../model/taxer'

describe 'taxer' do

  let(:taxer) { Taxer.new }  
   
  it 'el valor con impuestos de $1000 en UT deberia ser $1068.5' do
    expect(taxer.get_taxed_value(1000,'UT')).to eq 1068.5
  end

  it 'el valor con impuestos de $1000.50 en NV deberia ser $1080.54' do
    expect(taxer.get_taxed_value(1000.50,'NV')).to eq 1080.54
  end

  it 'el valor con impuestos de $1500 en TX deberia ser $1593.75' do
    expect(taxer.get_taxed_value(1500,'TX')).to eq 1593.75
  end

  it 'el valor con impuestos de $1000 en AL deberia ser $1040' do
    expect(taxer.get_taxed_value(1000,'AL')).to eq 1040
  end

  it 'el valor con impuestos de $1234 en CA deberia ser $1335.805' do
    expect(taxer.get_taxed_value(1234,'CA')).to eq 1335.805
  end

end
