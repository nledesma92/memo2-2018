require 'rspec' 
require_relative '../model/discounter'

describe 'discounter' do

  let(:discounter) { Discounter.new }  
   
  it 'si compro 3 unidades de $500 el valor total con descuento deberia ser $1455' do
    expect(discounter.get_total_with_discount(500,3)).to eq 1455
  end

  it 'si compro 3 unidades de $2000 el valor total con descuento deberia ser $5700' do
    expect(discounter.get_total_with_discount(2000,3)).to eq 5700
  end

  it 'si compro 3 unidades de $3000 el valor total con descuento deberia ser $8370' do
    expect(discounter.get_total_with_discount(3000,3)).to eq 8370
  end

  it 'si compro 2 unidades de $5000 el valor total con descuento deberia ser $9000' do
    expect(discounter.get_total_with_discount(5000,2)).to eq 9000
  end

  it 'si compro 3 unidades de $20000 el valor total con descuento deberia ser $51000' do
    expect(discounter.get_total_with_discount(20000,3)).to eq 51000
  end

end
