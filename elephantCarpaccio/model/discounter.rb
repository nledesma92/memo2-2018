class Discounter

    @@boundary1 = 1000
    @@rate_over1 = 0.03

    @@boundary2 = 5000
    @@rate_over2 = 0.05

    @@boundary3 = 7000
    @@rate_over3 = 0.07

    @@boundary4 = 10000
    @@rate_over4 = 0.1

    @@boundary5 = 50000
    @@rate_over5 = 0.15

    def get_total_with_discount(value, amount)
        total = value * amount
        rate = 0
        if total >= @@boundary5
            rate = @@rate_over5
        elsif total >= @@boundary4
            rate = @@rate_over4
        elsif total >= @@boundary3
            rate = @@rate_over3
        elsif total >= @@boundary2
            rate = @@rate_over2
        elsif total >= @@boundary1
            rate = @@rate_over1
        end
        total = total * (1 - rate)
    end
end