class Taxer

    @@tax_rates = { 
      'UT' => 0.0685, 
      'NV' => 0.08,
      'TX' => 0.0625,
      'AL' => 0.04,
      'CA' => 0.0825
    }
  
    def get_taxed_value(value, state_code)
      return value * (1 + @@tax_rates[state_code])
    end
  end