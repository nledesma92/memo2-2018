require 'sinatra'
require 'json'
require_relative './model/discounter'
require_relative './model/taxer'

set :bind, '0.0.0.0' 

get '/total_neto' do
  discounter = Discounter.new
  taxer = Taxer.new

  price = params["precio"]
  amount = params["cantidad"]
  state = params["estado"]

  total = discounter.get_total_with_discount(price.to_f, amount.to_i)
  total = taxer.get_taxed_value(total, state)

  content_type :json
  { 
    :total => total, 
    :detalle => {
      :precio => price.to_f,
      :cantidad => amount.to_i,
      :estado => state
    } 
  }.to_json
end