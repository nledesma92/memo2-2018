class Chopper

  def initialize
    @numeros = { '0' => 'cero', '1' => 'uno', '2' => 'dos', '3' => 'tres', '4' => 'cuatro',
      '5' => 'cinco', '6' => 'seis', '7' => 'siete', '8' => 'ocho', '9' => 'nueve' }
  end

  def chop(n, array)
    return (array.index(n) == nil)? -1 : array.index(n)
  end

  def sum(array)
    if (array.count == 0)
      return 'vacio'
    else
      result = getArraySumNumber(array)
      return getNumberSpanishString(result)
    end
  end

  def getArraySumNumber(array)
    sum = 0
    for element in array
      sum += element
    end
    return sum
  end

  def getNumberSpanishString(number)
    result = ''
    numberStringArray = number.to_s.split('')

    if(numberStringArray.count > 2)
      return 'demasiado grande'
     else 
      for i in 0..numberStringArray.count
        result +=  @numeros[numberStringArray[i]].to_s
        if(i<numberStringArray.count - 1)
          result +=','
        end
      end
      return result
    end
  end
end