class Parser

    def comma_separated_string_to_int_array(string)
        sum_args_array = string.split(/,/)
        sum_args_array.map! {|element| element.to_i}
    end
  
  end