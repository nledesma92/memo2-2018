# spec/app_spec.rb

require File.expand_path '../spec_helper.rb', __FILE__

NOT_FOUND = 404
SUCCESS = 200

describe "Sinatra Application" do

  it "get /sum?x=9,9 tiene que devolver status de exito, 'uno, ocho' como 'resultado', y 9,9 como parametro 'sum'" do
    expected_result =  'uno,ocho'
    expected_sum = '9,9'
    get '/sum?x=9,9'
    expect(last_response).to be_ok
    expect(last_response.status).to eq SUCCESS
    expect(JSON.parse(last_response.body)["sum"]).to eq expected_sum
    expect(JSON.parse(last_response.body)["resultado"]).to include expected_result
  end

  it "post /sum?x=9,9 tiene que devolver status not found, porque el metodo es incorrecto" do
    post '/sum?x=9,9'
    expect(last_response.ok?).to eq false
    expect(last_response.status).to eq NOT_FOUND
  end

  it "post /chop con parametros x=3 y=0,7,3 tiene que devolver status de exito, 2 en 'resultado', y x=3,y=0,7,3 en 'chop'" do
    expected_result = 2
    expected_chop = 'x=3,y=0,7,3'
    post '/chop', { 'x': '3', 'y': '0,7,3'}
    expect(last_response).to be_ok
    expect(last_response.status).to eq SUCCESS
    expect(JSON.parse(last_response.body)["chop"]).to eq expected_chop
    expect(JSON.parse(last_response.body)["resultado"]).to eq expected_result
  end

  it "get /chop?x=3&y=0,3,7 tiene que devolver status not found, porque el metodo es incorrecto" do
    get '/chop?x=3&y=0,3,7'
    post '/sum?x=9,9'
    expect(last_response.ok?).to eq false
    expect(last_response.status).to eq NOT_FOUND
  end
end