require 'sinatra'
require 'json'
require_relative './model/chopper'
require_relative './utils/parser'

set :bind, '0.0.0.0' 

get '/sum' do
  # Parsing
  sum_args = params["x"]
  parser = Parser.new
  sum_arg_array = parser.comma_separated_string_to_int_array(sum_args)

  # Sum
  chopper = Chopper.new
  result = chopper.sum(sum_arg_array)

  content_type :json
  { :sum => sum_args, :resultado => result}.to_json
end

post '/chop' do

  # Parsing
  x_arg = params["x"]
  y_arg = params["y"]
  parser = Parser.new

  x_arg_int = x_arg.to_i
  y_arg_array = parser.comma_separated_string_to_int_array(y_arg)
  
  # Chop
  chopper = Chopper.new
  result = chopper.chop(x_arg_int, y_arg_array)

  chop_args = 'x=' + x_arg + ',' + 'y='+ y_arg
  content_type :json
  { :chop => chop_args, :resultado => result}.to_json
end