class Chopper

  def initialize
    @numeros = { '0' => 'cero', '1' => 'uno', '2' => 'dos', '3' => 'tres', '4' => 'cuatro',
      '5' => 'cinco', '6' => 'seis', '7' => 'siete', '8' => 'ocho', '9' => 'nueve' }
  end

  def chop(n, array)
    return (array.index(n) == nil)? -1 : array.index(n)
  end

  def sum(array)
    if (array.count == 0)
      return 'vacio'
    else
      result = get_array_sum_number(array)
      return get_number_spanish_string(result)
    end
  end

  def get_array_sum_number(array)
    sum = 0
    for element in array
      sum += element
    end
    return sum
  end

  def get_number_spanish_string(number)
    result = ''
    num_string_array = number.to_s.split('')

    if(num_string_array.count > 2)
      return 'demasiado grande'
     else 
      for i in 0..num_string_array.count
        result +=  @numeros[num_string_array[i]].to_s
        if(i<num_string_array.count - 1)
          result +=','
        end
      end
      return result
    end
  end
end